All registers below are considered general purpose registers:

32-bit Registers:

eax- Accumulator register, used for IO, arithmetic, and other common instructions
ebx- Base register, used as a memory pointer and also interrupt return values

edi- Destination index register for strings and array operations
esi- Source index register for string and array operations

64-bit Registers:

rbx- Essentially the same as ebx but extended to accomodate 64 bits

rdi- Register destination index, destination for data copies
rsi- Register source index, source for data copies
