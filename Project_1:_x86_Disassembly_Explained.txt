L9: Starting point and function name defined as “entry_point”
L10: Decrement stack pointer and push rbx on the stack, to allocate 64-bits into the stack since we are using a 64-bit compiler and int is 32-bit
L11: Call the rand function and store it in eax, so it can be used for variable a
L12: Take the result from the rand function in eax and store it in ebx as variable a, since eax will be used to store the next input
L13: Call the rand function and store it in eax, so it can be used for variable b
L14: Take the result from the rand function in eax and store it in edi, the first parameter for sum2
L15: Take ebx, which holds variable a, and store it in esi, the second parameter for sum2
L16: Call sum2 and store the result in eax, so it can be used later to print the result

L1: Function name defined as sum2
L2: Add up the two parameter registers, rdi and rsi, and place the resulting address into eax
L3: Transfer control back to the entry point

L17: Take eax, the result of sum2, and move it to edi, where it can be used a parameter for print_the_value()
L18: Deallocating 64-bit from the stack to clear the stack
L19: Transfer control to the print_the_value function to print the value stored in edi as a string

L4: Function name defined as print_the_value
L5: Copy the parameter in edi, which holds the sum, into esi where it can be used as the value parameter for printf()
L6: Copy the memory address of the string “%d” into edi, to be used as the format parameter for printf()
L7: Clear the register eax by using xor on itself because we always want to set eax to some value which we choose as 0, just like how we sometimes return 0 for int functions
L8: Transfer control to the printf function with esi as the value to be printed and edi as “%d”, the printing format
